package com.etmall.unittestdemo

import android.util.Log
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GameTest {
    private var g : Game? = null // 宣告參考變數，指向 Game instance

    @Before
    @Throws(Exception::class)
    fun setUp() {
        g = Game()

//        mockkStatic(Log::class)
//        every { Log.i(any(), any()) } returns 0
//        every { Log.e(any(), any()) } returns 0
//        every { Log.d(any(), any()) } returns 0
//        every { Log.v(any(), any()) } returns 0
    }

    @Test // 測試玩家投球共20次洗溝 (gutter)時的總得分數
    // 期望結果值:0
    @Throws(Exception::class)
    fun testGutterGame() {
        val expected = 0 // 期望結果值
        val actual: Int // 實際結果值
        rollMany(20, 0)
        actual = g!!.score()

        // 斷言比對
        Assert.assertEquals(expected.toLong(), actual.toLong())
    }

    @Test // 測試玩家在所有全倒的12次投球的總得分數(滿分300分)
    @Throws(Exception::class)
    fun testPerfectGame() {
        val expected = 300
        val actual: Int
        rollMany(12, 10)
        actual = g!!.score()
        Assert.assertEquals(expected.toLong(), actual.toLong())
    }

    @Test // 測試玩家投球共20次，每次都只得一分的總得分數
    // 期望結果值:20
    @Throws(Exception::class)
    fun testAllOnes() {
        val expected = 20
        val actual: Int
        rollMany(20, 1)
        actual = g!!.score()

        // 斷言比對
        Assert.assertEquals(expected.toLong(), actual.toLong())
    }

    @Test // 測試玩家整局只有一次spare投球的分數
    // 共20次投球機會，其餘17次皆0分
    @Throws(Exception::class)
    fun testOneSpare() {
        val expected = 16
        val actual: Int
        rollSPare() //補中
        g!!.roll(3)
        rollMany(17, 0) //其餘17次投球皆0分
        actual = g!!.score()
        Assert.assertEquals(expected.toLong(), actual.toLong())
    }

    private fun rollSPare() {
        g!!.roll(5)
        g!!.roll(5)
    }

    @Test // 測試玩家整局只有一次strike投球的分數
    // 共19次投球機會，其餘16次皆0分
    @Throws(Exception::class)
    fun testOneStrike() {
        val expected = 24
        val actual: Int
        rollStrike()
        g!!.roll(3)
        g!!.roll(4)
        rollMany(16, 0)
        actual = g!!.score()
        Assert.assertEquals(expected.toLong(), actual.toLong())
    }

    private fun rollStrike() {
        g!!.roll(10)
    }

    private fun rollMany(n: Int, pins: Int) {
        for (i in 0 until n) g!!.roll(pins)
    }
}