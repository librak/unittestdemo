package com.etmall.unittestdemo

import android.app.Activity
import android.view.View

/** 參考資料：https://github.com/kenming/kata-bowling-game-by-java
 * 一. 10個計分格，每個計分格最多有兩次投球的機會
 * 二. 第一次投球就打倒全部球瓶，那該次的記分格就記 strike，該次計分格的分數為擊倒十瓶的10分再加上後面兩次丟球所打倒的球瓶分數
 * 三. 若第一次沒有全倒，則可以再投第二次，且如果把剩下的球瓶都擊倒就記 spare，該次計分格的分數需要擊倒十瓶的10分再加上後面一次丟球所打倒的球瓶分數
 * 四. 第十個計分格若是 spare 就會再多出一次丟球的機會，這樣才能決定第十個計分格的分數
 */

class Game {
    private val rolls = IntArray(21)
    private var currentRoll = 0
    fun roll(pins: Int) {
        rolls[currentRoll++] = pins
    }

    fun score(): Int {
        var score = 0
        var frameIndex = 0
        for (frame in 0..9) {
            if (isStrike(frameIndex)) {
                score += 10 + strikeBonus(frameIndex)
                frameIndex++
            } else if (isSpare(frameIndex)) {
                score += 10 + spareBonus(frameIndex)
                frameIndex += 2
            } else {
                score += sumOfBallsInFrame(frameIndex)
                frameIndex += 2
            }
        }
        return score
    }

    private fun isStrike(frameIndex: Int): Boolean {
        return rolls[frameIndex] == 10
    }

    private fun strikeBonus(frameIndex: Int): Int {
        return rolls[frameIndex + 1] + rolls[frameIndex + 2]
    }

    private fun isSpare(frameIndex: Int): Boolean {
        return rolls[frameIndex] + rolls[frameIndex + 1] == 10
    }

    private fun spareBonus(frameIndex: Int): Int {
        return rolls[frameIndex + 2]
    }

    private fun sumOfBallsInFrame(frameIndex: Int): Int {
        return rolls[frameIndex] + rolls[frameIndex + 1]
    }

}